

all: helloWorld

helloWorld: helloWorld.o
	$(CC) $(CFLAGS) -o helloWorld helloWorld.o

helloWorld.o: hello-world.c
	$(CC) $(CFLAGS) -c hello-world.c -o helloWorld.o

clean:
	rm helloWorld.o helloWorld
