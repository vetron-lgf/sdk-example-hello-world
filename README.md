Hello World - Example application for the Yocto SDK Tutorial
============================================================


#Directions

    #clone repo
    git clone https://bitbucket.org/vetron-lgf/sdk-example-hello-world 

    #change to project directory
    cd sdk-example-hello-world

    #make the application with environment settings
    make

    #test the app. NOTE: Will not work with host machine if using in SDK environment. Transfer binary to target device to test properly
    ./helloWorld

    #remove build artefacts
    make clean




